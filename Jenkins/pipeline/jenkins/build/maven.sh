#!/bin/bash

echo "********************"
echo "*****Building*******"
echo "********************"

WORKSPACE=/home/bigjib/Jenkins/jenkins_home/workspace/pipeline-maven

docker run --rm -v $WORKSPACE/java-app:/app -v /root/.m2/:/root/.m2/ -w /app maven:latest "$@"
