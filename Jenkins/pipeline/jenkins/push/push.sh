#!/bin/bash

echo "************************"
echo "**** Pushing image *****"
echo "************************"

IMAGE=maven-project 

echo "**** Logging In ********"
docker login -u bigjib -p $PASS

echo "***Tagging Image********"
docker tag $IMAGE:$BUILD_TAG bigjib/$IMAGE:$BUILD_TAG
echo "****Pushing Image ******"
docker push bigjib/$IMAGE:$BUILD_TAG