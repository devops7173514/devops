#!/bin/bash

echo maven-project > /tmp/.auth
echo $BUILD_TAG >> /tmp/.auth
echo $PASS >> /tmp/.auth

scp -o StrictHostKeyChecking=no -i ~/keys /tmp/.auth prod-user@172.21.44.95:/tmp/.auth
ssh -o StrictHostKeyChecking=no -i ~/keys prod-user@172.21.44.95 "cd maven && ./publish.sh"